# PubSweet Development Kit

For working on PubSweet core components in combination with PubSweet based apps

## Setup
Just run `sh setup.sh` in the root of repo.
Uncomment any repos you care about from that file.

## Gotchas

* Running `yarn` from the root will update and link all workspaces but running
  `yarn` from a workspace will update only that workspace and unlink it from the
  others. Sometimes this is what you want, see below.
* When adding a dependency to a workspace, remember to run `yarn` in that
  workspace before committing to update the `yarn.lock`.
* The above doesn't work for repos which do not define their own workspaces 
  (e.g. `pubsweet-starter`). This makes it hard to update the `yarn.lock`. For
  this reason, `xpub` defines an empty list of workspaces in `package.json`.
* Try to keep package versions in sync between workspaces in order to prevent
  duplication in the dependency tree. React and Jest have both been observed to
  cause problems in this regard (see below).

## Troubleshooting

- Having multiple copies of the same dependency can lead to errors with obscure
  messages. If you encounter one of these, try running `yarn list <name of
  dependency>` and then upgrading packages as necessary to deduplicate the
  dependency tree.
  
  * React
  
    _Only a ReactOwner can have refs. You might be removing a ref to a component
    that was not created inside a component's `render` method, or you have
    multiple copies of React loaded._
  
  * Jest
  
    _TypeError: Cannot read property 'bind' of undefined_
  
  * Joi
  
    _you must provide a joi schema_

  * Enzyme
    
    Enzyme Internal Error: configured enzyme adapter did not inherit from the EnzymeAdapter base class

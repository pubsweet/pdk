#!/bin/bash
set -e

[ -e pubsweet ] || git clone git@gitlab.coko.foundation:pubsweet/pubsweet.git
# [ -e editoria ] || git clone git@gitlab.coko.foundation:editoria/editoria.git
# [ -e pubsweet-starter ] || git clone git@gitlab.coko.foundation:pubsweet/pubsweet-starter.git
# [ -e wax ] || git clone git@gitlab.coko.foundation:wax/wax.git
# [ -e xpub ] || git clone git@gitlab.coko.foundation:xpub/xpub.git
# [ -e xpub-elife ] || git clone git@github.com:elifesciences/elife-xpub.git

# the first time, we need to run yarn in each folder
# so that husky installs its git hooks in the right place
for dir in */; do yarn --cwd $dir; done

# then run it again at the root to do the linking
yarn
